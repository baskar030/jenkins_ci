pipeline {
  agent {
    label "jenkins-maven"
  }
  environment {
    ORG = 'baskar030'
    APP_NAME = 'jenkins-ci'
    CHARTMUSEUM_CREDS = credentials('jenkins-x-chartmuseum')
  }
  stages {
    stage('CI Build and push snapshot') {
      when {
        branch 'PR-*'
      }
      environment {
        PREVIEW_VERSION = "0.0.0-SNAPSHOT-$BRANCH_NAME-$BUILD_NUMBER"
        PREVIEW_NAMESPACE = "$APP_NAME-$BRANCH_NAME".toLowerCase()
        HELM_RELEASE = "$PREVIEW_NAMESPACE".toLowerCase()
      }
      steps {
        container('maven') {
          sh "mvn versions:set -DnewVersion=$PREVIEW_VERSION"
          sh "mvn install"
          sh "skaffold version"
          sh "export VERSION=$PREVIEW_VERSION && skaffold build -f skaffold.yaml"
          sh "jx step post build --image $DOCKER_REGISTRY/$ORG/$APP_NAME:$PREVIEW_VERSION"
          dir('charts/preview') {
            sh "make preview"
            sh "jx preview --app $APP_NAME --dir ../.."
          }
        }
      }
    }
    stage('Git Checkout Dev Code') {
      when {
        anyOf{
            branch 'dev-testing'
        }
      }
      steps {
        container('maven') {
          // ensure we're not on a detached head
          sh "git checkout dev-testing"
        }
      }
    }
    stage('Git Checkout Master') {
          when {
            anyOf{
                branch 'master'
            }
          }
          steps {
            container('maven') {
              // ensure we're not on a detached head
              sh "git checkout master"
            }
          }
        }
    stage('Build Release') {
      when {
        anyOf{
            branch 'master';branch 'dev-testing'
        }
      }
      steps {
        container('maven') {

          // ensure we're not on a detached head
          //sh "git checkout master"
          sh "git config --global credential.helper store"
          sh "jx step git credentials"

          // so we can retrieve the version in later steps
          sh "echo \$(jx-release-version) > VERSION"
          sh "mvn versions:set -DnewVersion=\$(cat VERSION)"
          sh "jx step tag --version \$(cat VERSION)"
          sh "mvn clean deploy"
          sh "skaffold version"
          sh "export VERSION=`cat VERSION` && skaffold build -f skaffold.yaml"
          sh "jx step post build --image $DOCKER_REGISTRY/$ORG/$APP_NAME:\$(cat VERSION)"
        }
      }
    }
    stage('Promote to Environments') {
      when {
          anyOf{
              branch 'master';branch 'dev-testing'
          }
      }
      steps {
        container('maven') {
          dir('charts/jenkins-ci') {
            sh "jx step changelog --version v\$(cat ../../VERSION)"

            // release the helm chart
            sh "jx step helm release"

            // promote through all 'Auto' promotion Environments
            sh "jx promote -b --all-auto --timeout 1h --version \$(cat ../../VERSION)"
          }
        }
      }
    }
  }
  post {
        //to clean the jx workspace and to test the message -test
        always {
          cleanWs()
          emailext body: '''$PROJECT_NAME - Build # $BUILD_NUMBER - $BUILD_STATUS:
          Check console output at $BUILD_URL to view the results.''', subject: '$PROJECT_NAME - Build # $BUILD_NUMBER - $BUILD_STATUS!', to: 'vivek.tamilarasan@ionixxtech.com,ezrajohnson.paularul@ionixxtech.com,baskar.natarajan@ionixxtech.com'
        }
  }
}
